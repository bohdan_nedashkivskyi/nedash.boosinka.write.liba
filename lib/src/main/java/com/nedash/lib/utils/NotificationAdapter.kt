package com.nedash.lib.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.nedash.lib.R
import com.nedash.lib.data.model.AdDataDto
import com.nedash.lib.ui.NotificationActivity
import com.nedash.lib.utils.Constants.IntentExtraParams.TIME
import com.nedash.lib.utils.Constants.IntentExtraParams.URL
import com.nedash.lib.utils.Constants.NotificationAdapterData.NOTIFICATION_CHANNEL_ID
import com.nedash.lib.utils.Constants.NotificationAdapterData.NOTIFICATION_ID
import com.nedash.lib.utils.Constants.NotificationAdapterData.NOTIFICATION_NAME
import com.nedash.lib.utils.Constants.NotificationAdapterData.NOTIFICATION_TITLE

object NotificationAdapter {
    suspend fun showAdByNotification(context: Context, adDataDto: AdDataDto, time: Long) {
        val notificationManager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
            && notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
            createNotificationChannel(context)
        }
        val intent = Intent(context, NotificationActivity::class.java)
        intent.putExtra(URL, adDataDto.chromeLink)
        intent.putExtra(TIME, time)
        val notificationBuilder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(NOTIFICATION_TITLE)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_baseline_ad_units_24)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setFullScreenIntent(
                    PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT,

                        ), true
                ).build()

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder)
    }

    private fun createNotificationChannel(ctx: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )

            channel.lockscreenVisibility = Notification.VISIBILITY_SECRET

            val notificationManager: NotificationManager =
                ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}