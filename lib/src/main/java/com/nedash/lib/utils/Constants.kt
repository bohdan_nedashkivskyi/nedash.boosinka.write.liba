package com.nedash.lib.utils

import com.nedash.lib.BuildConfig

object Constants {

    val DEBUG_MODE = BuildConfig.DEBUG

    const val PROJECT_LOG_TAG = "boosinka.write.liba"
    const val SHARED_NAME: String = "fdghjdfhg"

    object SharedPrefKeys {
        const val MAIN_SHARED_PREF_KEY = "MAIN_SHARED_PREF_KEY"

        const val TIME_INSTALL_SHARED_PREF_KEY = "TIME_INSTALL_SHARED_PREF_KEY"
        const val TIME_LAST_AD_SHOWED_SHARED_PREF_KEY = "TIME_LAST_AD_SHOWED_SHARED_PREF_KEY"
        const val COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY = "COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY"
        const val COUNT_BROWSER_OPENED_SHARED_PREF_KEY = "COUNT_BROWSER_OPENED"
    }

    object Events {
        const val EVENTS_CONST_BROWSER_OPEN = "CHROME_OPEN"
        const val EVENTS_CONST_BLOCK_BY_FIRST_DELAY = "LOCK_BY_FIRST_DELAY"
        const val EVENTS_CONST_BLOCK_BY_DELAY = "LOCK_BY_DELAY"
        const val EVENTS_CONST_BLOCK_BY_AD_OUTER = "LOCK_BY_OUTER_AD"
        const val EVENTS_CONST_BLOCK_BY_VERSION_ON_REVIEW = "LOCK_BY_VERSION_ON_REVIEW"
        const val EVENTS_CONST_FIREBASE_SUBSCRIBE = "FIREBASE_SUBSCRIBE"
        const val EVENTS_CONST_FIREBASE_RECEIVE = "FIREBASE_RECEIVE"
        const val EVENTS_CONST_SCREEN_UNLOCK = "SCREEN_UNLOCKED"
        const val EVENTS_CONST_CONFIG_EMPTY = "CONFIG_NULL"
    }

    object NotificationAdapterData{
        const val NOTIFICATION_ID = 911
        const val NOTIFICATION_CHANNEL_ID = "REKLAMA"
        const val NOTIFICATION_TITLE = "RERLAMA"
        const val NOTIFICATION_CHANNEL = "wiadomosc"
        const val NOTIFICATION_NAME = "REKLAMA"
    }

    object IntentExtraParams{
        const val TIME = "TIME"
        const val URL = "URL"
    }
}