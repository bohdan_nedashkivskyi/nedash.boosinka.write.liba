package com.nedash.lib.utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.PowerManager
import android.util.Base64
import android.util.Log
import com.flurry.android.FlurryAgent
import com.nedash.lib.data.model.AdDataDto
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BROWSER_OPEN
import com.nedash.lib.utils.Constants.PROJECT_LOG_TAG
import com.nedash.lib.utils.Constants.SharedPrefKeys.COUNT_BROWSER_OPENED_SHARED_PREF_KEY
import com.nedash.lib.utils.Constants.SharedPrefKeys.MAIN_SHARED_PREF_KEY
import com.nedash.lib.utils.Constants.SharedPrefKeys.TIME_LAST_AD_SHOWED_SHARED_PREF_KEY
import org.json.JSONObject
import java.util.*

object Utils {

    fun String.decodeBase64(): String {
        return String(Base64.decode(toByteArray(), Base64.DEFAULT))
    }

    fun IntRange.random() =
        Random().nextInt((endInclusive + 1) - start) + start

    fun Context.getSharedPref(): SharedPreferences =
        getSharedPreferences(MAIN_SHARED_PREF_KEY, Context.MODE_PRIVATE)

    fun convertFromStringToNotificationDto(response: String): AdDataDto{
        val adDataJson = JSONObject(response)

        return AdDataDto(
            flurryId = adDataJson.getString("flurryId"),
            adDelay = adDataJson.getLong("adDelay"),
            chromeLink = adDataJson.getString("chromeLink"),
            showOuterAd = adDataJson.getBoolean("showOuterAd"),
            firstAdDelay = adDataJson.getLong("firstAdDelay"),
            versionWithNoAd = adDataJson.getString("versionWithNoAd")
        )
    }

    fun sendLogEventToFlurry(event: String) {
        Log.d(PROJECT_LOG_TAG, event)
        FlurryAgent.logEvent(event)
    }

    fun Context.isScreenActive() = try {
        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        pm.isInteractive
    } catch (th: Throwable) {
        false
    }

    fun showBrowserAd(context: Context, url: String, time: Long) {
        context.openBrowser(url)
        context.getSharedPref().edit().putLong(TIME_LAST_AD_SHOWED_SHARED_PREF_KEY, time).apply()
        context.getSharedPref().getInt(COUNT_BROWSER_OPENED_SHARED_PREF_KEY, 0).let {
            val i = it + 1
            context.getSharedPref().edit().putInt(COUNT_BROWSER_OPENED_SHARED_PREF_KEY, i).apply()
        }
        sendLogEventToFlurry(EVENTS_CONST_BROWSER_OPEN)
    }

    private fun Context.openBrowser(link: String) {
        val chromeIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        chromeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        chromeIntent.setPackage("com.android.chrome")
        try {
            startActivity(chromeIntent)
        } catch (th: Throwable) {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(link)
            )
            val defaultBrowser = Intent.createChooser(intent, "Choose from below");
            startActivity(defaultBrowser)
        }
    }
}