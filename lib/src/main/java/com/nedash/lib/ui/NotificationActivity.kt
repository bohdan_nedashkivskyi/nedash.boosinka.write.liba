package com.nedash.lib.ui

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.nedash.lib.utils.Constants.IntentExtraParams.TIME
import com.nedash.lib.utils.Constants.IntentExtraParams.URL
import com.nedash.lib.utils.Constants.NotificationAdapterData.NOTIFICATION_ID
import com.nedash.lib.utils.Utils.showBrowserAd

class NotificationActivity: AppCompatActivity() {

    private var handler: Handler? = null
    private var finish = Runnable { finish() }

    private var time: Long = 0
    private var url: String = ""

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        time = intent.getLongExtra(TIME, 0L)
        intent.getStringExtra(URL)?.let { url = it }

        handler = Handler(Looper.getMainLooper())
        handler?.postDelayed(finish, 3000)

        showBrowserAd(this, url, time)
        cancelNotification(this)
    }

    private fun cancelNotification(context: Context) {
        val notificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
    }

    override fun onDestroy() {
        handler?.removeCallbacks(finish)
        super.onDestroy()
    }

}